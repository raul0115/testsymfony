<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218154157 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE appointments_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_BDC9C7AE5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE causes_external (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_CD2B2A265E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities (id INT AUTO_INCREMENT NOT NULL, departament_id INT NOT NULL, code VARCHAR(25) NOT NULL, name VARCHAR(50) NOT NULL, INDEX IDX_D95DB16B48B3EEE4 (departament_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departaments (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(25) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diagnostics (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(10) NOT NULL, symbole VARCHAR(255) DEFAULT NULL, description VARCHAR(100) NOT NULL, sex VARCHAR(4) NOT NULL, lower_limit INT NOT NULL, upper_limit INT NOT NULL, primary_condition TINYINT(1) NOT NULL, observation VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctor_consultants (id INT AUTO_INCREMENT NOT NULL, doctor_id INT NOT NULL, address LONGTEXT NOT NULL, coordinate LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', main TINYINT(1) NOT NULL, INDEX IDX_3DA4FA0C87F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctor_files (id INT AUTO_INCREMENT NOT NULL, doctor_id INT NOT NULL, electronic_signature VARCHAR(255) DEFAULT NULL, electronic_stamp VARCHAR(255) DEFAULT NULL, INDEX IDX_4BC1A03F87F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctors_favorites (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, doctor_id INT NOT NULL, INDEX IDX_1264F5696B899279 (patient_id), INDEX IDX_1264F56987F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctor_specialties (id INT AUTO_INCREMENT NOT NULL, specialty_id INT NOT NULL, doctor_id INT NOT NULL, main TINYINT(1) NOT NULL, INDEX IDX_C638E04B9A353316 (specialty_id), INDEX IDX_C638E04B87F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctor_studies (id INT AUTO_INCREMENT NOT NULL, type_study_id INT DEFAULT NULL, doctor_id INT NOT NULL, university_degree VARCHAR(255) NOT NULL, university VARCHAR(255) NOT NULL, graduation_date INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_500479922A4F2415 (type_study_id), INDEX IDX_5004799287F4FB17 (doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE durations_appointments (id INT AUTO_INCREMENT NOT NULL, duration INT NOT NULL, UNIQUE INDEX UNIQ_CBC0BEAD865F80C0 (duration), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE eps (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_576E89A95E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exams (id INT AUTO_INCREMENT NOT NULL, code_iss VARCHAR(20) NOT NULL, cups VARCHAR(20) NOT NULL, description VARCHAR(255) NOT NULL, saiva_group VARCHAR(75) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE family_members (id INT AUTO_INCREMENT NOT NULL, patient_id INT DEFAULT NULL, family_id INT NOT NULL, type_family_ties_id INT NOT NULL, main TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_28B406436B899279 (patient_id), INDEX IDX_28B40643C35E566A (family_id), INDEX IDX_28B40643203F6391 (type_family_ties_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE general_physical_states (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_454EF4DC5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE incapacities_classes (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_6259B6245E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE maritals_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_E06B321C5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE medicines (id INT AUTO_INCREMENT NOT NULL, product VARCHAR(255) NOT NULL, expedient INT NOT NULL, holder LONGTEXT NOT NULL, description VARCHAR(255) DEFAULT NULL, state_cum INT NOT NULL, atc VARCHAR(15) NOT NULL, atc_description VARCHAR(50) NOT NULL, administration_way VARCHAR(15) NOT NULL, active_principle VARCHAR(150) NOT NULL, unit_measurement VARCHAR(10) NOT NULL, quantity NUMERIC(7, 2) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payments_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_35A3FB6C5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permissions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_2DEDCC6F5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission_role (id INT AUTO_INCREMENT NOT NULL, role_id INT DEFAULT NULL, permission_id INT DEFAULT NULL, INDEX IDX_6A711CAD60322AC (role_id), INDEX IDX_6A711CAFED90CCA (permission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profiles (id INT AUTO_INCREMENT NOT NULL, province_id INT DEFAULT NULL, marital_status_id INT DEFAULT NULL, eps_id INT DEFAULT NULL, role_id INT NOT NULL, status_id INT NOT NULL, profile_id INT DEFAULT NULL, type_document_id INT DEFAULT NULL, email VARCHAR(120) NOT NULL, uid VARCHAR(50) DEFAULT NULL, first_name VARCHAR(15) DEFAULT NULL, second_name VARCHAR(15) DEFAULT NULL, first_surname VARCHAR(15) DEFAULT NULL, second_surname VARCHAR(15) DEFAULT NULL, main_phone VARCHAR(15) DEFAULT NULL, doctor_phone VARCHAR(15) DEFAULT NULL, birth_date DATE DEFAULT NULL, gender VARCHAR(10) DEFAULT NULL, document_number VARCHAR(50) DEFAULT NULL, profile_image VARCHAR(150) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, mail_doctor VARCHAR(120) DEFAULT NULL, medical_id VARCHAR(50) DEFAULT NULL, medical_summary LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8B308530E7927C74 (email), UNIQUE INDEX UNIQ_8B308530592AF3BA (medical_id), INDEX IDX_8B308530E946114A (province_id), INDEX IDX_8B308530E559F9BF (marital_status_id), INDEX IDX_8B308530F0A3CCD5 (eps_id), INDEX IDX_8B308530D60322AC (role_id), INDEX IDX_8B3085306BF700BD (status_id), UNIQUE INDEX UNIQ_8B308530CCFA12B8 (profile_id), INDEX IDX_8B3085308826AFA6 (type_document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provinces (id INT AUTO_INCREMENT NOT NULL, departament_id INT NOT NULL, city_id INT NOT NULL, code VARCHAR(25) NOT NULL, name VARCHAR(50) NOT NULL, INDEX IDX_8C96CC5748B3EEE4 (departament_id), INDEX IDX_8C96CC578BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purposes_consults (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_97F5CE6F5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, description VARCHAR(255) DEFAULT NULL, active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_B63E2EC75E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialties (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, name VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_410754B05E237E06 (name), INDEX IDX_410754B06BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE states_conscience (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_D0CE87735E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_7B00651C5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_documents (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_7EA918A05E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_family_ties (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_D7D4FFCD5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_payments (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_AAB57C1C5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_score (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_936BE31D5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_studies (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_1EDBAD095E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cities ADD CONSTRAINT FK_D95DB16B48B3EEE4 FOREIGN KEY (departament_id) REFERENCES departaments (id)');
        $this->addSql('ALTER TABLE doctor_consultants ADD CONSTRAINT FK_3DA4FA0C87F4FB17 FOREIGN KEY (doctor_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE doctor_files ADD CONSTRAINT FK_4BC1A03F87F4FB17 FOREIGN KEY (doctor_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE doctors_favorites ADD CONSTRAINT FK_1264F5696B899279 FOREIGN KEY (patient_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE doctors_favorites ADD CONSTRAINT FK_1264F56987F4FB17 FOREIGN KEY (doctor_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE doctor_specialties ADD CONSTRAINT FK_C638E04B9A353316 FOREIGN KEY (specialty_id) REFERENCES specialties (id)');
        $this->addSql('ALTER TABLE doctor_specialties ADD CONSTRAINT FK_C638E04B87F4FB17 FOREIGN KEY (doctor_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE doctor_studies ADD CONSTRAINT FK_500479922A4F2415 FOREIGN KEY (type_study_id) REFERENCES types_studies (id)');
        $this->addSql('ALTER TABLE doctor_studies ADD CONSTRAINT FK_5004799287F4FB17 FOREIGN KEY (doctor_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE family_members ADD CONSTRAINT FK_28B406436B899279 FOREIGN KEY (patient_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE family_members ADD CONSTRAINT FK_28B40643C35E566A FOREIGN KEY (family_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE family_members ADD CONSTRAINT FK_28B40643203F6391 FOREIGN KEY (type_family_ties_id) REFERENCES types_family_ties (id)');
        $this->addSql('ALTER TABLE permission_role ADD CONSTRAINT FK_6A711CAD60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE permission_role ADD CONSTRAINT FK_6A711CAFED90CCA FOREIGN KEY (permission_id) REFERENCES permissions (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B308530E946114A FOREIGN KEY (province_id) REFERENCES provinces (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B308530E559F9BF FOREIGN KEY (marital_status_id) REFERENCES maritals_status (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B308530F0A3CCD5 FOREIGN KEY (eps_id) REFERENCES eps (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B308530D60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B3085306BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B308530CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profiles (id)');
        $this->addSql('ALTER TABLE profiles ADD CONSTRAINT FK_8B3085308826AFA6 FOREIGN KEY (type_document_id) REFERENCES types_documents (id)');
        $this->addSql('ALTER TABLE provinces ADD CONSTRAINT FK_8C96CC5748B3EEE4 FOREIGN KEY (departament_id) REFERENCES departaments (id)');
        $this->addSql('ALTER TABLE provinces ADD CONSTRAINT FK_8C96CC578BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
        $this->addSql('ALTER TABLE specialties ADD CONSTRAINT FK_410754B06BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provinces DROP FOREIGN KEY FK_8C96CC578BAC62AF');
        $this->addSql('ALTER TABLE cities DROP FOREIGN KEY FK_D95DB16B48B3EEE4');
        $this->addSql('ALTER TABLE provinces DROP FOREIGN KEY FK_8C96CC5748B3EEE4');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B308530F0A3CCD5');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B308530E559F9BF');
        $this->addSql('ALTER TABLE permission_role DROP FOREIGN KEY FK_6A711CAFED90CCA');
        $this->addSql('ALTER TABLE doctor_consultants DROP FOREIGN KEY FK_3DA4FA0C87F4FB17');
        $this->addSql('ALTER TABLE doctor_files DROP FOREIGN KEY FK_4BC1A03F87F4FB17');
        $this->addSql('ALTER TABLE doctors_favorites DROP FOREIGN KEY FK_1264F5696B899279');
        $this->addSql('ALTER TABLE doctors_favorites DROP FOREIGN KEY FK_1264F56987F4FB17');
        $this->addSql('ALTER TABLE doctor_specialties DROP FOREIGN KEY FK_C638E04B87F4FB17');
        $this->addSql('ALTER TABLE doctor_studies DROP FOREIGN KEY FK_5004799287F4FB17');
        $this->addSql('ALTER TABLE family_members DROP FOREIGN KEY FK_28B406436B899279');
        $this->addSql('ALTER TABLE family_members DROP FOREIGN KEY FK_28B40643C35E566A');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B308530CCFA12B8');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B308530E946114A');
        $this->addSql('ALTER TABLE permission_role DROP FOREIGN KEY FK_6A711CAD60322AC');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B308530D60322AC');
        $this->addSql('ALTER TABLE doctor_specialties DROP FOREIGN KEY FK_C638E04B9A353316');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B3085306BF700BD');
        $this->addSql('ALTER TABLE specialties DROP FOREIGN KEY FK_410754B06BF700BD');
        $this->addSql('ALTER TABLE profiles DROP FOREIGN KEY FK_8B3085308826AFA6');
        $this->addSql('ALTER TABLE family_members DROP FOREIGN KEY FK_28B40643203F6391');
        $this->addSql('ALTER TABLE doctor_studies DROP FOREIGN KEY FK_500479922A4F2415');
        $this->addSql('DROP TABLE appointments_status');
        $this->addSql('DROP TABLE causes_external');
        $this->addSql('DROP TABLE cities');
        $this->addSql('DROP TABLE departaments');
        $this->addSql('DROP TABLE diagnostics');
        $this->addSql('DROP TABLE doctor_consultants');
        $this->addSql('DROP TABLE doctor_files');
        $this->addSql('DROP TABLE doctors_favorites');
        $this->addSql('DROP TABLE doctor_specialties');
        $this->addSql('DROP TABLE doctor_studies');
        $this->addSql('DROP TABLE durations_appointments');
        $this->addSql('DROP TABLE eps');
        $this->addSql('DROP TABLE exams');
        $this->addSql('DROP TABLE family_members');
        $this->addSql('DROP TABLE general_physical_states');
        $this->addSql('DROP TABLE incapacities_classes');
        $this->addSql('DROP TABLE maritals_status');
        $this->addSql('DROP TABLE medicines');
        $this->addSql('DROP TABLE payments_status');
        $this->addSql('DROP TABLE permissions');
        $this->addSql('DROP TABLE permission_role');
        $this->addSql('DROP TABLE profiles');
        $this->addSql('DROP TABLE provinces');
        $this->addSql('DROP TABLE purposes_consults');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE specialties');
        $this->addSql('DROP TABLE states_conscience');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE types_documents');
        $this->addSql('DROP TABLE types_family_ties');
        $this->addSql('DROP TABLE types_payments');
        $this->addSql('DROP TABLE types_score');
        $this->addSql('DROP TABLE types_studies');
    }
}
