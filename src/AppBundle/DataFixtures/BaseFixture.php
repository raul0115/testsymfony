<?php

namespace AppBundle\DataFixtures;

use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

abstract class BaseFixture extends Fixture
{
    private $manager;
    protected $faker;
    public function load(ObjectManager $manager)

    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadData($manager);
    
    }
    protected function createMany(string $className, int $count, callable $factory)

    {
        for ($i = 0; $i < $count; $i++) {
            $entity = new $className();
            $factory($entity);
            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            //$this->addReference($className . '_' . $i, $entity);
        }
       
    }

    abstract protected function loadData(ObjectManager $manager);

}