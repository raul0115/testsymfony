<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Role;
use AppBundle\Entity\Status;
use AppBundle\Entity\Profile;
use AppBundle\Entity\Specialty;
use AppBundle\Entity\TypeDocument;
use AppBundle\Entity\TypeStudy;
use AppBundle\Entity\Eps;
use AppBundle\Entity\Permission;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager)

    {
        $role1 = new Role();
        $role1->setName('Admin');
        $role1->setDescription('admin');
        $role1->setActive(1);

        $role2 = new Role();
        $role2->setName('Medico');
        $role2->setDescription('medico');
        $role2->setActive(1);

        $role3 = new Role();
        $role3->setName('Paciente');
        $role3->setDescription('paciente');
        $role3->setActive(1);

        $status1 = new Status();
        $status1->setName('Registrado');

        $status2 = new Status();
        $status2->setName('Visible');
       
        $status3 = new Status();
        $status3->setName('No Visible');

        $eps1 = new Eps();
        $eps1->setName('Famisanar');

        $eps2 = new Eps();
        $eps2->setName('Colsanitas');

        $eps3 = new Eps();
        $eps3->setName('Compensar');

        $typeDocument1 = new TypeDocument();
        $typeDocument1->setName('Cedula');

        $typeDocument2 = new TypeDocument();
        $typeDocument2->setName('Pasaporte');

        $specialty1 = new Specialty();
        $specialty1->setName('Cirujano');

        $specialty2 = new Specialty();
        $specialty2->setName('Oftalmologo');

        $typeStudy1 = new TypeStudy();
        $typeStudy1->setName('Pregrado');

        $typeStudy2 = new TypeStudy();
        $typeStudy2->setName('Postgrado');

        $manager->persist($role1);
        $manager->persist($role2);
        $manager->persist($role3);

        $manager->persist($status1);
        $manager->persist($status2);
        $manager->persist($status3);

        $manager->persist($eps1);
        $manager->persist($eps2);
        $manager->persist($eps3);

        $manager->persist($typeDocument1);
        $manager->persist($typeDocument2);
        $manager->persist($specialty1);
        $manager->persist($specialty2);
        $manager->persist($typeStudy1);
        $manager->persist($typeStudy2);

        $this->createMany(Profile::class, 25, function(Profile $doctor) use ($role2,$status2){

          $doctor->setEmail($this->faker->email);
          $doctor->setRole($role2);
          $doctor->setStatus($status2);
         
        });
        $this->createMany(Profile::class, 25, function(Profile $patient) use ($role3,$status2){

            $patient->setEmail($this->faker->email);
            $patient->setRole($role3);
            $patient->setStatus($status2);
           
          });

        $manager->flush();
    }
}