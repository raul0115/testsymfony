<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorSpecialty
 *
 * @ORM\Table(name="doctor_specialties")
 * @ORM\Entity
 */
class DoctorSpecialty
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $main;

    /**
     * @var \AppBundle\Entity\Specialty
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialty")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specialty_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $specialty;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile", inversedBy="doctorSpecialties")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $doctor;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set main.
     *
     * @param bool $main
     *
     * @return DoctorSpecialty
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main.
     *
     * @return bool
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Get the value of doctor
     *
     * @return  \AppBundle\Entity\Profile
     */ 
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set the value of doctor
     *
     * @param  \AppBundle\Entity\Profile  $doctor
     *
     * @return DoctorSpecialty
     */ 
    public function setDoctor(\AppBundle\Entity\Profile $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get the value of specialty
     *
     * @return  \AppBundle\Entity\Specialty
     */ 
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Set the value of specialty
     *
     * @param  \AppBundle\Entity\Specialty  $specialty
     *
     * @return DoctorSpecialty
     */ 
    public function setSpecialty(\AppBundle\Entity\Specialty $specialty)
    {
        $this->specialty = $specialty;

        return $this;
    }
}
