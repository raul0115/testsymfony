<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Exam
 *
 * @ORM\Table(name="exams")
 * @ORM\Entity
 */
class Exam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_iss", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $code_iss;

    /**
     * @var string
     *
     * @ORM\Column(name="cups", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $cups;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="saiva_group", type="string", length=75, nullable=false, unique=false)
     */
    private $saiva_group;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeIss.
     *
     * @param string $codeIss
     *
     * @return Exam
     */
    public function setCodeIss($codeIss)
    {
        $this->code_iss = $codeIss;

        return $this;
    }

    /**
     * Get codeIss.
     *
     * @return string
     */
    public function getCodeIss()
    {
        return $this->code_iss;
    }

    /**
     * Set cups.
     *
     * @param string $cups
     *
     * @return Exam
     */
    public function setCups($cups)
    {
        $this->cups = $cups;

        return $this;
    }

    /**
     * Get cups.
     *
     * @return string
     */
    public function getCups()
    {
        return $this->cups;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Exam
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set saivaGroup.
     *
     * @param string $saivaGroup
     *
     * @return Exam
     */
    public function setSaivaGroup($saivaGroup)
    {
        $this->saiva_group = $saivaGroup;

        return $this;
    }

    /**
     * Get saivaGroup.
     *
     * @return string
     */
    public function getSaivaGroup()
    {
        return $this->saiva_group;
    }
}
