<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorFavorite
 *
 * @ORM\Table(name="doctors_favorites")
 * @ORM\Entity
 */
class DoctorsFavorite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="patient_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $patient;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $doctor;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set patient.
     *
     * @param \AppBundle\Entity\Profile|null $patient
     *
     * @return DoctorFavorite
     */
    public function setPatient(\AppBundle\Entity\Profile $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient.
     *
     * @return \AppBundle\Entity\Profile|null
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set doctor.
     *
     * @param \AppBundle\Entity\Profile|null $doctor
     *
     * @return DoctorFavorite
     */
    public function setDoctor(\AppBundle\Entity\Profile $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor.
     *
     * @return \AppBundle\Entity\Profile|null
     */
    public function getDoctor()
    {
        return $this->doctor;
    }
}
