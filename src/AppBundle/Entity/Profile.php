<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profile
 *
 * @ORM\Table(name="profiles")
 * @ORM\Entity 
 * @ORM\HasLifecycleCallbacks 
 */
class Profile
{
    const GENDER_MALE = 'Masculino';
    const GENDER_FEMALE = 'Femenino';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=120, precision=0, scale=0, nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uid", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $uid;


    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $first_name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="second_name", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $second_name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_surname", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $first_surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="second_surname", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $second_surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="main_phone", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $main_phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="doctor_phone", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $doctor_phone;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="birth_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $birth_date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gender", type="string", length=10, precision=0, scale=0, nullable=true, unique=false)
     */
    private $gender;

    /**
     * @var string|null
     *
     * @ORM\Column(name="document_number", type="string", length=50, precision=0, scale=0, nullable=true, unique=false)
     */
    private $document_number;

    /**
     * @var string|null
     *
     * @ORM\Column(name="profile_image", type="string", length=150, precision=0, scale=0, nullable=true, unique=false)
     */
    private $profile_image;

     /**
     * @var \AppBundle\Entity\Province|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Province")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="province_id", referencedColumnName="id")
     * })
     */
    private $province;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mail_doctor", type="string", length=120, precision=0, scale=0, nullable=true, unique=false)
     */
    private $mail_doctor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="medical_id", type="string", length=50, precision=0, scale=0, nullable=true, unique=true)
     */
    private $medical_id;

     /**
     * @var string|null
     *
     * @ORM\Column(name="medical_summary", type="text", nullable=true, unique=false)
     */
    private $medical_summary;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\MaritalStatus
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MaritalStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marital_status_id", referencedColumnName="id")
     * })
     */
    private $marital_status;

    /**
     * @var \AppBundle\Entity\Eps
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Eps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eps_id", referencedColumnName="id")
     * })
     */
    private $eps;

    /**
     * @var \AppBundle\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Role")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var \AppBundle\Entity\Status
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Status")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="profile_id", referencedColumnName="id", unique=true)
     * })
     */
    private $profile;

    /**
     * @var \AppBundle\Entity\TypeDocument
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeDocument")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_document_id", referencedColumnName="id")
     * })
     */
    private $type_document;


    /**
    * @var \Doctrine\Common\Collections\Collection
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\DoctorFile", mappedBy="doctor")
    */
    private $doctor_files;

    /**
    * @var \Doctrine\Common\Collections\Collection
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\DoctorStudy", mappedBy="doctor")
    */
    private $doctor_studies;

    /**
    * @var \Doctrine\Common\Collections\Collection
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\DoctorSpecialty", mappedBy="doctor")
    */
    private $doctor_specialties;

    /**
    * @var \Doctrine\Common\Collections\Collection
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\DoctorConsultant", mappedBy="doctor")
    */
    private $doctor_consultants;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
        $this->doctor_files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->doctor_studies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->doctor_specialties = new \Doctrine\Common\Collections\ArrayCollection();
        $this->doctor_consultants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Profile
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return Profile
     */
    public function setFirstName($firstName = null)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set secondName.
     *
     * @param string|null $secondName
     *
     * @return Profile
     */
    public function setSecondName($secondName = null)
    {
        $this->second_name = $secondName;

        return $this;
    }

    /**
     * Get secondName.
     *
     * @return string|null
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Set firstSurname.
     *
     * @param string|null $firstSurname
     *
     * @return Profile
     */
    public function setFirstSurname($firstSurname = null)
    {
        $this->first_surname = $firstSurname;

        return $this;
    }

    /**
     * Get firstSurname.
     *
     * @return string|null
     */
    public function getFirstSurname()
    {
        return $this->first_surname;
    }

    /**
     * Set secondSurname.
     *
     * @param string|null $secondSurname
     *
     * @return Profile
     */
    public function setSecondSurname($secondSurname = null)
    {
        $this->second_surname = $secondSurname;

        return $this;
    }

    /**
     * Get secondSurname.
     *
     * @return string|null
     */
    public function getSecondSurname()
    {
        return $this->second_surname;
    }

    /**
     * Set mainPhone.
     *
     * @param string|null $mainPhone
     *
     * @return Profile
     */
    public function setMainPhone($mainPhone = null)
    {
        $this->main_phone = $mainPhone;

        return $this;
    }

    /**
     * Get mainPhone.
     *
     * @return string|null
     */
    public function getMainPhone()
    {
        return $this->main_phone;
    }

    /**
     * Set doctorPhone.
     *
     * @param string|null $doctorPhone
     *
     * @return Profile
     */
    public function setDoctorPhone($doctorPhone = null)
    {
        $this->doctor_phone = $doctorPhone;

        return $this;
    }

    /**
     * Get doctorPhone.
     *
     * @return string|null
     */
    public function getDoctorPhone()
    {
        return $this->doctor_phone;
    }

    /**
     * Set birthDate.
     *
     * @param \DateTime|null $birthDate
     *
     * @return Profile
     */
    public function setBirthDate($birthDate = null)
    {
        $this->birth_date = $birthDate;

        return $this;
    }

    /**
     * Get birthDate.
     *
     * @return \DateTime|null
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set gender.
     *
     * @param string|null $gender
     *
     * @return Profile
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set documentNumber.
     *
     * @param string|null $documentNumber
     *
     * @return Profile
     */
    public function setDocumentNumber($documentNumber = null)
    {
        $this->document_number = $documentNumber;

        return $this;
    }

    /**
     * Get documentNumber.
     *
     * @return string|null
     */
    public function getDocumentNumber()
    {
        return $this->document_number;
    }

    /**
     * Set profileImage.
     *
     * @param string|null $profileImage
     *
     * @return Profile
     */
    public function setProfileImage($profileImage = null)
    {
        if (!is_null($profileImage)) {
            $this->profile_image = $profileImage;
        }

        return $this;
    }

    /**
     * Get profileImage.
     *
     * @return string|null
     */
    public function getProfileImage()
    {
        return $this->profile_image;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return Profile
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set mailDoctor.
     *
     * @param string|null $mailDoctor
     *
     * @return Profile
     */
    public function setMailDoctor($mailDoctor = null)
    {
        $this->mail_doctor = $mailDoctor;

        return $this;
    }

    /**
     * Get mailDoctor.
     *
     * @return string|null
     */
    public function getMailDoctor()
    {
        return $this->mail_doctor;
    }

    /**
     * Set medicalId.
     *
     * @param string|null $medicalId
     *
     * @return Profile
     */
    public function setMedicalId($medicalId = null)
    {
        $this->medical_id = $medicalId;

        return $this;
    }

    /**
     * Get medicalId.
     *
     * @return string|null
     */
    public function getMedicalId()
    {
        return $this->medical_id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Profile
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Profile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set maritalStatus.
     *
     * @param \AppBundle\Entity\MaritalStatus|null $maritalStatus
     *
     * @return Profile
     */
    public function setMaritalStatus(\AppBundle\Entity\MaritalStatus $maritalStatus = null)
    {
        $this->marital_status = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus.
     *
     * @return \AppBundle\Entity\MaritalStatus|null
     */
    public function getMaritalStatus()
    {
        return $this->marital_status;
    }

    /**
     * Set eps.
     *
     * @param \AppBundle\Entity\Eps|null $eps
     *
     * @return Profile
     */
    public function setEps(\AppBundle\Entity\Eps $eps = null)
    {
        $this->eps = $eps;

        return $this;
    }

    /**
     * Get eps.
     *
     * @return \AppBundle\Entity\Eps|null
     */
    public function getEps()
    {
        return $this->eps;
    }

    /**
     * Set role.
     *
     * @param \AppBundle\Entity\Role|null $role
     *
     * @return Profile
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role.
     *
     * @return \AppBundle\Entity\Role|null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set status.
     *
     * @param \AppBundle\Entity\Status|null $status
     *
     * @return Profile
     */
    public function setStatus(\AppBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \AppBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set profile.
     *
     * @param \AppBundle\Entity\Profile|null $profile
     *
     * @return Profile
     */
    public function setProfile(\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile.
     *
     * @return \AppBundle\Entity\Profile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set typeDocument.
     *
     * @param \AppBundle\Entity\TypeDocument|null $typeDocument
     *
     * @return Profile
     */
    public function setTypeDocument(\AppBundle\Entity\TypeDocument $typeDocument = null)
    {
        $this->type_document = $typeDocument;

        return $this;
    }

    /**
     * Get typeDocument.
     *
     * @return \AppBundle\Entity\TypeDocument|null
     */
    public function getTypeDocument()
    {
        return $this->type_document;
    }

    /**
     * Get the value of uid
     *
     * @return  string|null
     */ 
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set the value of uid
     *
     * @param  string|null  $uid
     *
     * @return Profile
     */ 
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get the value of province
     *
     * @return  \AppBundle\Entity\Province|null
     */ 
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set the value of province
     *
     * @param  \AppBundle\Entity\Province|null  $province
     *
     * @return Profile
     */ 
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Add doctorFile.
     *
     * @param \AppBundle\Entity\DoctorFile $doctorFile
     *
     * @return Profile
     */
    public function addDoctorFile(\AppBundle\Entity\DoctorFile $doctorFile)
    {
        $this->doctor_files[] = $doctorFile;

        return $this;
    }

    /**
     * Remove doctorFile.
     *
     * @param \AppBundle\Entity\DoctorFile $doctorFile
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorFile(\AppBundle\Entity\DoctorFile $doctorFile)
    {
        return $this->doctor_files->removeElement($doctorFile);
    }

    /**
     * Get doctorFiles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorFiles()
    {
        return $this->doctor_files;
    }
    /**
     * Get the value of medicalSummary
     *
     * @return  string|null
     */ 
    public function getMedicalSummary()
    {
        return $this->medical_summary;
    }
   
     /**
     * Add doctorStudy.
     *
     * @param \AppBundle\Entity\DoctorStudy $doctorStudy
     *
     * @return Profile
     */

    public function addDoctorStudy(\AppBundle\Entity\DoctorStudy $doctorStudy)
    {
        $this->doctor_studies[] = $doctorStudy;

        return $this;
    }

    /**
     * Remove doctorStudy.
     *
     * @param \AppBundle\Entity\DoctorStudy $doctorStudy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorStudy(\AppBundle\Entity\DoctorStudy $doctorStudy)
    {
        return $this->doctor_studies->removeElement($doctorStudy);
    }

    /**
     * Get doctorStudies.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorStudies()
    {
        return $this->doctor_studies;
    }

    /**
     * Add doctorSpecialty.
     *
     * @param \AppBundle\Entity\DoctorSpecialty $doctorSpecialty
     *
     * @return Profile
     */
    public function addDoctorSpecialty(\AppBundle\Entity\DoctorSpecialty $doctorSpecialty)
    {
        $this->doctor_specialties[] = $doctorSpecialty;

        return $this;
    }

    /**
     * Remove doctorSpecialty.
     *
     * @param \AppBundle\Entity\DoctorSpecialty $doctorSpecialty
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorSpecialty(\AppBundle\Entity\DoctorSpecialty $doctorSpecialty)
    {
        return $this->doctor_specialties->removeElement($doctorSpecialty);
    }

    /**
     * Get doctorSpecialties.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorSpecialties()
    {
        return $this->doctor_specialties;
    }

     /**
     * Add doctorConsultant.
     *
     * @param \AppBundle\Entity\DoctorConsultant $doctorConsultant
     *
     * @return Profile
     */
    public function addDoctorConsultant(\AppBundle\Entity\DoctorConsultant $doctorConsultant)
    {
        $this->doctor_consultants[] = $doctorConsultant;

        return $this;
    }

    /**
     * Remove doctorConsultant.
     *
     * @param \AppBundle\Entity\DoctorConsultant $doctorConsultant
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDoctorConsultant(\AppBundle\Entity\DoctorConsultant $doctorConsultant)
    {
        return $this->doctor_consultants->removeElement($doctorConsultant);
    }

    /**
     * Get doctorConsultants.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctorConsultants()
    {
        return $this->doctor_consultants;
    }

    /**
     * Set the value of medical_summary
     *
     * @param  string|null  $medical_summary
     *
     * @return Profile
     */ 
    public function setMedicalSummary($medical_summary)
    {
        $this->medical_summary = $medical_summary;

        return $this;
    }


    /**
    * Generates the magic method
    * 
    */
    public function __toString(){
        return ($this->email)? $this->email : '';
    }

     
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }
    
}
