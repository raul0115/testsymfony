<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FamilyMember
 *
 * @ORM\Table(name="family_members")
 * @ORM\Entity
 */
class FamilyMember
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean", nullable=false, unique=false)
     */
    private $main;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, unique=false)
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, unique=false)
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     * })
     */
    private $patient;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="family_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $family;

    /**
     * @var \AppBundle\Entity\TypeFamilyTies
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeFamilyTies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_family_ties_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $type_family_ties;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set main.
     *
     * @param bool $main
     *
     * @return FamilyMember
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main.
     *
     * @return bool
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return FamilyMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return FamilyMember
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set patient.
     *
     * @param \AppBundle\Entity\Profile|null $patient
     *
     * @return FamilyMember
     */
    public function setPatient(\AppBundle\Entity\Profile $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient.
     *
     * @return \AppBundle\Entity\Profile|null
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set family.
     *
     * @param \AppBundle\Entity\Profile|null $family
     *
     * @return FamilyMember
     */
    public function setFamily(\AppBundle\Entity\Profile $family = null)
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Get family.
     *
     * @return \AppBundle\Entity\Profile|null
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Set typeFamilyTies.
     *
     * @param \AppBundle\Entity\TypeFamilyTies|null $typeFamilyTies
     *
     * @return FamilyMember
     */
    public function setTypeFamilyTies(\AppBundle\Entity\TypeFamilyTies $typeFamilyTies = null)
    {
        $this->type_family_ties = $typeFamilyTies;

        return $this;
    }

    /**
     * Get typeFamilyTies.
     *
     * @return \AppBundle\Entity\TypeFamilyTies|null
     */
    public function getTypeFamilyTies()
    {
        return $this->type_family_ties;
    }
}
