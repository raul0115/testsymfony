<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="cities")
 * @ORM\Entity
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=25, nullable=false, unique=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var \AppBundle\Entity\Departament
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Departament")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departament_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $departament;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return City
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Get the value of departament
     *
     * @return  \AppBundle\Entity\Departament
     */ 
    public function getDepartament()
    {
        return $this->departament;
    }

    /**
     * Set the value of departament
     *
     * @param  \AppBundle\Entity\Departament  $departament
     *
     * @return  self
     */ 
    public function setDepartament(\AppBundle\Entity\Departament $departament)
    {
        $this->departament = $departament;

        return $this;
    }
}
