<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medicine
 *
 * @ORM\Table(name="medicines")
 * @ORM\Entity
 */
class Medicine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=255, nullable=false, unique=false)
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="expedient", type="integer", nullable=false, unique=false)
     */
    private $expedient;

    /**
     * @var string
     *
     * @ORM\Column(name="holder", type="text", nullable=false, unique=false)
     */
    private $holder;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="state_cum", type="integer", nullable=false, unique=false)
     */
    private $state_cum;

    /**
     * @var string
     *
     * @ORM\Column(name="atc", type="string", length=15, nullable=false, unique=false)
     */
    private $atc;

    /**
     * @var string
     *
     * @ORM\Column(name="atc_description", type="string", length=50, nullable=false, unique=false)
     */
    private $atc_description;

    /**
     * @var string
     *
     * @ORM\Column(name="administration_way", type="string", length=15, nullable=false, unique=false)
     */
    private $administration_way;

    /**
     * @var string
     *
     * @ORM\Column(name="active_principle", type="string", length=150, nullable=false, unique=false)
     */
    private $active_principle;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_measurement", type="string", length=10, nullable=false, unique=false)
     */
    private $unit_measurement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quantity", type="decimal", precision=7, scale=2, nullable=true, unique=false)
     */
    private $quantity;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product.
     *
     * @param string $product
     *
     * @return Medicine
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set expedient.
     *
     * @param int $expedient
     *
     * @return Medicine
     */
    public function setExpedient($expedient)
    {
        $this->expedient = $expedient;

        return $this;
    }

    /**
     * Get expedient.
     *
     * @return int
     */
    public function getExpedient()
    {
        return $this->expedient;
    }

    /**
     * Set holder.
     *
     * @param string $holder
     *
     * @return Medicine
     */
    public function setHolder($holder)
    {
        $this->holder = $holder;

        return $this;
    }

    /**
     * Get holder.
     *
     * @return string
     */
    public function getHolder()
    {
        return $this->holder;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Medicine
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stateCum.
     *
     * @param int $stateCum
     *
     * @return Medicine
     */
    public function setStateCum($stateCum)
    {
        $this->state_cum = $stateCum;

        return $this;
    }

    /**
     * Get stateCum.
     *
     * @return int
     */
    public function getStateCum()
    {
        return $this->state_cum;
    }

    /**
     * Set atc.
     *
     * @param string $atc
     *
     * @return Medicine
     */
    public function setAtc($atc)
    {
        $this->atc = $atc;

        return $this;
    }

    /**
     * Get atc.
     *
     * @return string
     */
    public function getAtc()
    {
        return $this->atc;
    }

    /**
     * Set atcDescription.
     *
     * @param string $atcDescription
     *
     * @return Medicine
     */
    public function setAtcDescription($atcDescription)
    {
        $this->atc_description = $atcDescription;

        return $this;
    }

    /**
     * Get atcDescription.
     *
     * @return string
     */
    public function getAtcDescription()
    {
        return $this->atc_description;
    }

    /**
     * Set administrationWay.
     *
     * @param string $administrationWay
     *
     * @return Medicine
     */
    public function setAdministrationWay($administrationWay)
    {
        $this->administration_way = $administrationWay;

        return $this;
    }

    /**
     * Get administrationWay.
     *
     * @return string
     */
    public function getAdministrationWay()
    {
        return $this->administration_way;
    }

    /**
     * Set activePrinciple.
     *
     * @param string $activePrinciple
     *
     * @return Medicine
     */
    public function setActivePrinciple($activePrinciple)
    {
        $this->active_principle = $activePrinciple;

        return $this;
    }

    /**
     * Get activePrinciple.
     *
     * @return string
     */
    public function getActivePrinciple()
    {
        return $this->active_principle;
    }

    /**
     * Set unitMeasurement.
     *
     * @param string $unitMeasurement
     *
     * @return Medicine
     */
    public function setUnitMeasurement($unitMeasurement)
    {
        $this->unit_measurement = $unitMeasurement;

        return $this;
    }

    /**
     * Get unitMeasurement.
     *
     * @return string
     */
    public function getUnitMeasurement()
    {
        return $this->unit_measurement;
    }

    /**
     * Set quantity.
     *
     * @param string|null $quantity
     *
     * @return Medicine
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
