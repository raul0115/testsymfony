<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorFile
 *
 * @ORM\Table(name="doctor_files")
 * @ORM\Entity
 */
class DoctorFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="electronic_signature", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $electronic_signature;

    /**
     * @var string|null
     *
     * @ORM\Column(name="electronic_stamp", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $electronic_stamp;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile", inversedBy="doctorFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $doctor;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set electronicSignature.
     *
     * @param string|null $electronicSignature
     *
     * @return DoctorFile
     */
    public function setElectronicSignature($electronicSignature = null)
    {
        $this->electronic_signature = $electronicSignature;

        return $this;
    }

    /**
     * Get electronicSignature.
     *
     * @return string|null
     */
    public function getElectronicSignature()
    {
        return $this->electronic_signature;
    }

    /**
     * Set electronicStamp.
     *
     * @param string|null $electronicStamp
     *
     * @return DoctorFile
     */
    public function setElectronicStamp($electronicStamp = null)
    {
        $this->electronic_stamp = $electronicStamp;

        return $this;
    }

    /**
     * Get electronicStamp.
     *
     * @return string|null
     */
    public function getElectronicStamp()
    {
        return $this->electronic_stamp;
    }

    /**
     * Get the value of doctor
     *
     * @return  \AppBundle\Entity\Profile
     */ 
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set the value of doctor
     *
     * @param  \AppBundle\Entity\Profile  $doctor
     *
     * @return DoctorFile
     */ 
    public function setDoctor(\AppBundle\Entity\Profile $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }
}
