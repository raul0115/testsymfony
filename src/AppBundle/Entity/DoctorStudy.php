<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorStudy
 *
 * @ORM\Table(name="doctor_studies")
 * @ORM\Entity
 */
class DoctorStudy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="university_degree", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $university_degree;

    /**
     * @var string
     *
     * @ORM\Column(name="university", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $university;

    /**
     * @var int
     *
     * @ORM\Column(name="graduation_date", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $graduation_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updated_at;

    /**
     * @var \AppBundle\Entity\TypeStudy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeStudy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_study_id", referencedColumnName="id")
     * })
     */
    private $type_study;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile", inversedBy="doctorStudies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $doctor;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set universityDegree.
     *
     * @param string $universityDegree
     *
     * @return DoctorStudy
     */
    public function setUniversityDegree($universityDegree)
    {
        $this->university_degree = $universityDegree;

        return $this;
    }

    /**
     * Get universityDegree.
     *
     * @return string
     */
    public function getUniversityDegree()
    {
        return $this->university_degree;
    }

    /**
     * Set university.
     *
     * @param string $university
     *
     * @return DoctorStudy
     */
    public function setUniversity($university)
    {
        $this->university = $university;

        return $this;
    }

    /**
     * Get university.
     *
     * @return string
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Set graduationDate.
     *
     * @param int $graduationDate
     *
     * @return DoctorStudy
     */
    public function setGraduationDate($graduationDate)
    {
        $this->graduation_date = $graduationDate;

        return $this;
    }

    /**
     * Get graduationDate.
     *
     * @return int
     */
    public function getGraduationDate()
    {
        return $this->graduation_date;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return DoctorStudy
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return DoctorStudy
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set typeStudy.
     *
     * @param \AppBundle\Entity\TypeStudy|null $typeStudy
     *
     * @return DoctorStudy
     */
    public function setTypeStudy(\AppBundle\Entity\TypeStudy $typeStudy = null)
    {
        $this->type_study = $typeStudy;

        return $this;
    }

    /**
     * Get typeStudy.
     *
     * @return \AppBundle\Entity\TypeStudy|null
     */
    public function getTypeStudy()
    {
        return $this->type_study;
    }

    /**
     * Get the value of doctor
     *
     * @return  \AppBundle\Entity\Profile
     */ 
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set the value of doctor
     *
     * @param  \AppBundle\Entity\Profile  $doctor
     *
     * @return  self
     */ 
    public function setDoctor(\AppBundle\Entity\Profile $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }
    
    /** @ORM\PreUpdate */
    public function doStuffOnPreUpdate()
    {
        $this->updated_at = new \DateTime();
    }
}
