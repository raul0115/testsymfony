<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermissionRole
 *
 * @ORM\Table(name="permission_role")
 * @ORM\Entity
 */
class PermissionRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Role", inversedBy="permissionRoles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var \AppBundle\Entity\Permission
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Permission", inversedBy="permissionRoles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
     * })
     */
    private $permission;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role.
     *
     * @param \AppBundle\Entity\Role|null $role
     *
     * @return PermissionRole
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role.
     *
     * @return \AppBundle\Entity\Role|null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set permission.
     *
     * @param \AppBundle\Entity\Permission|null $permission
     *
     * @return PermissionRole
     */
    public function setPermission(\AppBundle\Entity\Permission $permission = null)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission.
     *
     * @return \AppBundle\Entity\Permission|null
     */
    public function getPermission()
    {
        return $this->permission;
    }
}
