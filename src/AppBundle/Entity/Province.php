<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Province
 *
 * @ORM\Table(name="provinces")
 * @ORM\Entity
 */
class Province
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=25, nullable=false, unique=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var \AppBundle\Entity\Departament
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Departament")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departament_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $departament;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $city;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Province
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Province
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Get the value of departament
     *
     * @return  \AppBundle\Entity\Departament
     */ 
    public function getDepartament()
    {
        return $this->departament;
    }

    /**
     * Set the value of departament
     *
     * @param  \AppBundle\Entity\Departament  $departament
     *
     * @return  self
     */ 
    public function setDepartament(\AppBundle\Entity\Departament $departament)
    {
        $this->departament = $departament;

        return $this;
    }

    /**
     * Get the value of city
     *
     * @return  \AppBundle\Entity\City
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @param  \AppBundle\Entity\City  $city
     *
     * @return  self
     */ 
    public function setCity(\AppBundle\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }
}
