<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorConsultant
 *
 * @ORM\Table(name="doctor_consultants")
 * @ORM\Entity
 */
class DoctorConsultant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", precision=0, scale=0, nullable=false, unique=false)
     */
    private $address;

    /**
     * @var json|null
     *
     * @ORM\Column(name="coordinate", type="json", precision=0, scale=0, nullable=true, unique=false)
     */
    private $coordinate;

    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $main;

    /**
     * @var \AppBundle\Entity\Profile
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Profile", inversedBy="doctorConsultants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor_id", nullable=false, referencedColumnName="id")
     * })
     */
    private $doctor;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return DoctorConsultant
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set coordinate.
     *
     * @param json|null $coordinate
     *
     * @return DoctorConsultant
     */
    public function setCoordinate($coordinate = null)
    {
        $this->coordinate = $coordinate;

        return $this;
    }

    /**
     * Get coordinate.
     *
     * @return json|null
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Set main.
     *
     * @param bool $main
     *
     * @return DoctorConsultant
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main.
     *
     * @return bool
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Get the value of doctor
     *
     * @return  \AppBundle\Entity\Profile
     */ 
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set the value of doctor
     *
     * @param  \AppBundle\Entity\Profile  $doctor
     *
     * @return DoctorConsultant
     */ 
    public function setDoctor(\AppBundle\Entity\Profile $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }
}
