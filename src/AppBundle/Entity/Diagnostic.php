<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diagnostic
 *
 * @ORM\Table(name="diagnostics")
 * @ORM\Entity
 */
class Diagnostic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=false, unique=false)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="symbole", type="string", length=255, nullable=true, unique=false)
     */
    private $symbole;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100, nullable=false, unique=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=4, nullable=false, unique=false)
     */
    private $sex;

    /**
     * @var int
     *
     * @ORM\Column(name="lower_limit", type="integer", nullable=false, unique=false)
     */
    private $lower_limit;

    /**
     * @var int
     *
     * @ORM\Column(name="upper_limit", type="integer", nullable=false, unique=false)
     */
    private $upper_limit;

    /**
     * @var bool
     *
     * @ORM\Column(name="primary_condition", type="boolean", nullable=false, unique=false)
     */
    private $primary_condition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observation", type="string", length=255, nullable=true, unique=false)
     */
    private $observation;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Diagnostic
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set symbole.
     *
     * @param string|null $symbole
     *
     * @return Diagnostic
     */
    public function setSymbole($symbole = null)
    {
        $this->symbole = $symbole;

        return $this;
    }

    /**
     * Get symbole.
     *
     * @return string|null
     */
    public function getSymbole()
    {
        return $this->symbole;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Diagnostic
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sex.
     *
     * @param string $sex
     *
     * @return Diagnostic
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex.
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set lowerLimit.
     *
     * @param int $lowerLimit
     *
     * @return Diagnostic
     */
    public function setLowerLimit($lowerLimit)
    {
        $this->lower_limit = $lowerLimit;

        return $this;
    }

    /**
     * Get lowerLimit.
     *
     * @return int
     */
    public function getLowerLimit()
    {
        return $this->lower_limit;
    }

    /**
     * Set upperLimit.
     *
     * @param int $upperLimit
     *
     * @return Diagnostic
     */
    public function setUpperLimit($upperLimit)
    {
        $this->upper_limit = $upperLimit;

        return $this;
    }

    /**
     * Get upperLimit.
     *
     * @return int
     */
    public function getUpperLimit()
    {
        return $this->upper_limit;
    }

    /**
     * Set primaryCondition.
     *
     * @param bool $primaryCondition
     *
     * @return Diagnostic
     */
    public function setPrimaryCondition($primaryCondition)
    {
        $this->primary_condition = $primaryCondition;

        return $this;
    }

    /**
     * Get primaryCondition.
     *
     * @return bool
     */
    public function getPrimaryCondition()
    {
        return $this->primary_condition;
    }

    /**
     * Set observation.
     *
     * @param string|null $observation
     *
     * @return Diagnostic
     */
    public function setObservation($observation = null)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation.
     *
     * @return string|null
     */
    public function getObservation()
    {
        return $this->observation;
    }
}
