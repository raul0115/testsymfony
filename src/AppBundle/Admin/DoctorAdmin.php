<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Entity\Profile;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\Role;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
class DoctorAdmin extends AbstractAdmin
{
    protected $datagridValues = [

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'updated_at',
    ];
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

      
        $datagridMapper
            //->add('id')
            ->add('email')
            ->add('first_name')
            //->add('second_name')
            ->add('first_surname')
            //->add('second_surname')
            //->add('main_phone')
           // ->add('doctor_phone')
           // ->add('birth_date')
           // ->add('gender')
           // ->add('document_type')
           // ->add('document_number')
          // ->add('profile_image')
           // ->add('address')
            //->add('role')
            ->add('mail_doctor')
           ->add('medical_id')
            //->add('created_at')
            //->add('updated_at')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id')
            ->add('email')
            ->add('first_name')
            //->add('second_name')
            ->add('first_surname')
            //->add('second_surname')
            //->add('main_phone')
            //->add('doctor_phone')
            //->add('birth_date')
            //->add('gender')
            //->add('document_type')
            //->add('document_number')
           // ->add('profile_image')
           // ->add('address')
            ->add('mail_doctor')
            ->add('medical_id')
            //->add('role')
            ->add('status')
           // ->add('created_at')
            ->add('updated_at')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        //$profile = $this->getSubject();
        $formMapper
            //->add('id')
            ->add('status')
            ->add('email')
            ->add('first_name')
            ->add('second_name')
            ->add('first_surname')
            ->add('second_surname')
            ->add('main_phone')
            ->add('doctor_phone')
            ->add('birth_date')
            ->add('gender','choice',[
                'choices'=>[
                    Profile::GENDER_MALE => Profile::GENDER_MALE,
                    Profile::GENDER_FEMALE =>  Profile::GENDER_FEMALE
                ],
                'placeholder' => 'Choose an gender',
                'required'=>false
            ])
            ->add('type_document')
            ->add('document_number')
            ->add('profile_image', FileType::class, [
                'required' => false,
                'data_class' => null
            ])
            ->add('address')
            ->add('mail_doctor')
            ->add('medical_id')
           // ->add('created_at')
           // ->add('updated_at')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
           // ->add('id')
            ->add('email')
            ->add('first_name')
            ->add('second_name')
            ->add('first_surname')
            ->add('second_surname')
            ->add('main_phone')
            ->add('doctor_phone')
            ->add('birth_date')
            ->add('gender')
            ->add('type_document')
            ->add('document_number')
            ->add('profile_image')
            ->add('address')
            ->add('role')
            ->add('status')
            ->add('mail_doctor')
             ->add('medical_id')
            //->add('created_at')
           // ->add('updated_at')
        ;
    } 
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
        ->with('email')
            ->assertNotNull(array())
            ->assertNotBlank()
            ->assertEmail()
        ->end();

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $profile = $em->getRepository(Profile::class)->findOneBy(array('email' => $object->getEmail()));
        
        if($profile !== null && $object->getId() != $profile->getId()){
            $errorElement
            ->with('email')
            ->addViolation('Email must be unique!')
            ->end();
        }
    }
    public function prePersist($profile){
        $container = $this->getConfigurationPool()->getContainer();
        
        $em = $container->get('doctrine.orm.entity_manager');
        $role = $em->getRepository(Role::class)->findOneBy(array('name' => 'Medico'));
        $profile->setRole($role);
        $file_upload =  $container->get('app.file_uploader');
        $file = $profile->getProfileImage();
        $fileName= null;
        if($file instanceof UploadedFile){
            $fileName = $file_upload->upload($file);
        }
        $profile->setProfileImage($fileName);
    }
    public function preUpdate($profile)
    {
        $container = $this->getConfigurationPool()->getContainer();
        
        $file_upload =  $container->get('app.file_uploader');
        $file = $profile->getProfileImage();
        if($file instanceof UploadedFile){
            $fileName = $file_upload->upload($file);
            $profile->setProfileImage($fileName);
        }
        
        $profile->setUpdatedAt(new \DateTime());
        
    }

    
    public function toString($object)
    {
        return 'Doctors'; // shown in the breadcrumb on the create view
    }

    public function createQuery($context = 'role')
    {
        $query = parent::createQuery($context);
        $query->innerJoin( $query->getRootAliases()[0].'.role', 'ro', 'WITH', 'ro.name = :role');
        
        $query->setParameter('role','Medico');
        return $query;
    }
    
}
