<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use ApiBundle\Repository\ProfileRepository;

class DoctorStudyAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('doctor')
            ->add('type_study')
            ->add('university_degree')
            ->add('university')
            ->add('graduation_date')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('doctor')
            ->add('type_study')
            ->add('university_degree')
            ->add('university')
            ->add('graduation_date')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $container = $this->getConfigurationPool()->getContainer();
        
        $repProfile =  $container->get(ProfileRepository::class);
        $formMapper
            ->add('doctor')
            ->add('type_study')
            ->add('university_degree')
            ->add('university')
            ->add('graduation_date')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('doctor')
            ->add('university_degree')
            ->add('university')
            ->add('graduation_date')
            ->add('created_at')
            ->add('updated_at')
        ;
    }
}
