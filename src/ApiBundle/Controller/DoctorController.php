<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ApiBundle\Repository\ProfileRepository;
use Symfony\Component\HttpFoundation\Request;
use AppBunlde\Entity\Profile;

class DoctorController extends Controller
{

    /**
     * @var ProfileRepository
    */
    private $profileRepository;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    public function showAction(Request $request,$id)
    {
    
        $helpers = $this->get("api.helpers");
        $profile = $this->profileRepository->findByRole($id,2);;
        if(!is_null($profile)){
            $profile->setImage("https://www.multidoctores.com/Views/New/Home/Index/img/home-doctor.jpg");
                return $helpers->toJson('ok',$profile,null,['email',
                                                            'role',
                                                            'status',
                                                            'doctorFiles',
                                                            'uid',
                                                            'mainPhone',
                                                            'eps',
                                                            'profile',
                                                            'maritalStatus',
                                                            'province',
                                                            'typeDocument',
                                                            'documentNumber',
                                                            'doctor',
                                                            'typeStudy'

                                                            ]);
        }
        $errors =[];
        $errors['error']= "Not found doctor.";
        return $helpers->toJson('not_found',$errors);
    }
}
