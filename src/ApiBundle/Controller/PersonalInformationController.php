<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ApiBundle\Repository\PersonalInformationRepository;
use Symfony\Component\HttpFoundation\Request;

class PersonalInformationController extends Controller
{

    /**
    * @var PersonalInformationRepository
    */
    private $personalInformationR;

    public function __construct(PersonalInformationRepository $personalInformationR)
    {
        $this->personalInformationR = $personalInformationR;
    }

    public function indexAction(Request $request)
    {
    
        $helpers = $this->get("api.helpers");
        $personalInformation = $this->personalInformationR->findAll();
        return $helpers->toJson('ok',$personalInformation);
    }
}
