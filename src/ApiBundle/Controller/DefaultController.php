<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ApiBundle\Repository\RoleRepository;
use Symfony\Component\HttpFoundation\Request;
use ApiBundle\Form\RoleType;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @var RoleRepository
    */
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function indexAction()
    {
        $helpers = $this->get("api.helpers");
        //$roles =  $this->roleRepository->todos();
        return $helpers->toJson(['success'=>true]);
    }

    public function storeAction(Request $request)
    {
        
        $helpers = $this->get("api.helpers");
        $data = (array) json_decode($request->getContent());

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
        'name' => [new Assert\Length(array('min' => 2)),
                   new Assert\Length(array('max' => 3))],
        'description' =>[ new Assert\Length(array('min' => 100)),
                    new Assert\Email()]
        ));
        
        $violations = $validator->validate($data, $constraint);
        if(!$violations->count() > 0 ){
            return $helpers->toJson(['success'=>true]);
        }
        $errors = $helpers->getConstraintViolationList($violations);
        return $helpers->toJson(['errors'=>$errors], Response::HTTP_NOT_ACCEPTABLE);
    }
}
