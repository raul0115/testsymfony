<?php 
namespace ApiBundle\Request;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class UpdateProfilePatientRequest
{
    public $id;
    /**
     * @Assert\NotBlank
     */
    public $firstName;
     /**
     * @Assert\NotBlank
     */
    public $secondName;

     /**
     * @Assert\NotBlank
     *
     */
    public $firstSurname;

    public $email;
    
    public $secondSurname;

    /**
     * @Assert\Date
     * @var string A "yyyy-mm-dd" formatted value
     */
    public $birthDate;

    /**
    * @var \DateTime|null
    */
    public $createdAt;

    /**
    * @var \DateTime|null
    */
    public $updatedAt;
    
    /**
     * @Assert\NotBlank
     *
     */
    public $gender;

     /**
     * @var \AppBundle\Entity\Eps|null
     */
    public $eps;

    /**
     * @var \AppBundle\Entity\TypeDocument|null
     *
     */
    public $typeDocument;

    /**
     * @var string|null
     *
     */
    public $documentNumber;
    
    /**
     * @var string|null
     *
     */
    public $mainPhone;

    /**
    * @var string|null
    *
    */
    public $profileImage;

    /**
     * Get the value of secondName
     */ 
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set the value of secondName
     *
     * @return  self
     */ 
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get the value of firstName
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of firstSurname
     */ 
    public function getFirstSurname()
    {
        return $this->firstSurname;
    }

    /**
     * Set the value of firstSurname
     *
     * @return  self
     */ 
    public function setFirstSurname($firstSurname)
    {
        $this->firstSurname = $firstSurname;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of secondSurname
     */ 
    public function getSecondSurname()
    {
        return $this->secondSurname;
    }

    /**
     * Set the value of secondSurname
     *
     * @return  self
     */ 
    public function setSecondSurname($secondSurname)
    {
        $this->secondSurname = $secondSurname;

        return $this;
    }

    /**
     * Get the value of birthDate
     */ 
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set the value of birthDate
     *
     * @return  self
     */ 
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the value of gender
     */ 
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set the value of gender
     *
     * @return  self
     */ 
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @return  self
     */ 
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of eps
     *
     * @return  \AppBundle\Entity\Eps|null
     */ 
    public function getEps()
    {
        return $this->eps;
    }

    /**
     * Set the value of eps
     *
     * @param  \AppBundle\Entity\Eps|null  $eps
     *
     * @return  self
     */ 
    public function setEps($eps)
    {
        $this->eps = $eps;

        return $this;
    }

    /**
     * Get the value of typeDocument
     *
     * @return  \AppBundle\Entity\TypeDocument|null
     */ 
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }

    /**
     * Set the value of typeDocument
     *
     * @param  \AppBundle\Entity\TypeDocument|null  $typeDocument
     *
     * @return  self
     */ 
    public function setTypeDocument($typeDocument)
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    /**
     * Get the value of documentNumber
     *
     * @return  string|null
     */ 
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Set the value of documentNumber
     *
     * @param  string|null  $documentNumber
     *
     * @return  self
     */ 
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }
    
    /**
     * Get the value of mainPhone
     *
     * @return  string|null
     */ 
    public function getMainPhone()
    {
        return $this->mainPhone;
    }

    /**
     * Set the value of mainPhone
     *
     * @param  string|null  $mainPhone
     *
     * @return  self
     */ 
    public function setMainPhone($mainPhone)
    {
        $this->mainPhone = $mainPhone;

        return $this;
    }

    /**
    * @Assert\Callback
    */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if( !is_null($this->getTypeDocument()) && !empty($this->getTypeDocument()) && (is_null($this->getDocumentNumber()) || empty($this->getDocumentNumber()))){
            $context->buildViolation('This value should not be blank.')
            ->atPath('documentNumber')
            ->addViolation();
        }
        if( !is_null($this->getDocumentNumber()) && !empty($this->getDocumentNumber()) && (is_null($this->getTypeDocument()) || empty($this->getTypeDocument()))){
            $context->buildViolation('This value should not be blank.')
            ->atPath('typeDocument')
            ->addViolation();
        }
      
    }

    

    /**
     * Get the value of profileImage
     *
     * @return  string|null
     */ 
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set the value of profileImage
     *
     * @param  string|null  $profileImage
     *
     * @return  self
     */ 
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;

        return $this;
    }
}