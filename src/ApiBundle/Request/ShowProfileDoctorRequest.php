<?php 
namespace ApiBundle\Request;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ShowProfileDoctorRequest
{
    public $id;
    /**
    * @var string|null
    */
    public $firstName;
    /**
    * @var string|null
    */
    public $secondName;

    /**
    * @var string|null
    */
    public $firstSurname;

    /**
    * @var string|null
    */
    public $mail_doctor;
    
    /**
    * @var string|null
    */
    public $secondSurname;

    /**
    * @var \Date|null
    */
    public $birthDate;

    /**
    * @var \DateTime|null
    */
    public $createdAt;

    /**
    * @var \DateTime|null
    */
    public $updatedAt;
    
    /**
    *@var int|null
    */
    public $gender;
    
    /**
    * @var string|null
    *
    */
    public $doctorPhone;
    
    /**
    * @var \Doctrine\Common\Collections\Collection|null
    *
    */
    public $doctorSpecialties;
    
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of firstName
     *
     * @return  string|null
     */ 
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param  string|null  $firstName
     *
     * @return  self
     */ 
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of secondName
     *
     * @return  string|null
     */ 
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set the value of secondName
     *
     * @param  string|null  $secondName
     *
     * @return  self
     */ 
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get the value of firstSurname
     *
     * @return  string|null
     */ 
    public function getFirstSurname()
    {
        return $this->firstSurname;
    }

    /**
     * Set the value of firstSurname
     *
     * @param  string|null  $firstSurname
     *
     * @return  self
     */ 
    public function setFirstSurname($firstSurname)
    {
        $this->firstSurname = $firstSurname;

        return $this;
    }

    /**
     * Get the value of mail_doctor
     *
     * @return  string|null
     */ 
    public function getMail_doctor()
    {
        return $this->mail_doctor;
    }

    /**
     * Set the value of mail_doctor
     *
     * @param  string|null  $mail_doctor
     *
     * @return  self
     */ 
    public function setMail_doctor($mail_doctor)
    {
        $this->mail_doctor = $mail_doctor;

        return $this;
    }

    /**
     * Get the value of secondSurname
     *
     * @return  string|null
     */ 
    public function getSecondSurname()
    {
        return $this->secondSurname;
    }

    /**
     * Set the value of secondSurname
     *
     * @param  string|null  $secondSurname
     *
     * @return  self
     */ 
    public function setSecondSurname($secondSurname)
    {
        $this->secondSurname = $secondSurname;

        return $this;
    }

    /**
     * Get the value of birthDate
     *
     * @return  \Date|null
     */ 
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set the value of birthDate
     *
     * @param  \Date|null  $birthDate
     *
     * @return  self
     */ 
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the value of createdAt
     *
     * @return  \DateTime|null
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @param  \DateTime|null  $createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     *
     * @return  \DateTime|null
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @param  \DateTime|null  $updatedAt
     *
     * @return  self
     */ 
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get *@var int|null
     */ 
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set *@var int|null
     *
     * @return  self
     */ 
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get the value of doctorPhone
     *
     * @return  string|null
     */ 
    public function getDoctorPhone()
    {
        return $this->doctorPhone;
    }

    /**
     * Set the value of doctorPhone
     *
     * @param  string|null  $doctorPhone
     *
     * @return  self
     */ 
    public function setDoctorPhone($doctorPhone)
    {
        $this->doctorPhone = $doctorPhone;

        return $this;
    }

    /**
     * Get the value of doctorSpecialties
     *
     * @return  \Doctrine\Common\Collections\Collection|null
     */ 
    public function getDoctorSpecialties()
    {
        return $this->doctorSpecialties;
    }

    /**
     * Set the value of doctorSpecialties
     *
     * @param  \Doctrine\Common\Collections\Collection|null  $doctorSpecialties
     *
     * @return  self
     */ 
    public function setDoctorSpecialties($doctorSpecialties)
    {
        $this->doctorSpecialties = $doctorSpecialties;

        return $this;
    }
}