<?php 
declare(strict_types=1);

namespace ApiBundle\Repository;

use AppBundle\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class RoleRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Role::class);
    }
    public function todos(){
        return $this->repository->findAll();
       // return $this->repository->find(1)->getPermissions()->toArray();
    }
    public function saludar()
    {
        return 'hola';
    }
}