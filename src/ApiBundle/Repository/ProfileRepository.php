<?php 
declare(strict_types=1);

namespace ApiBundle\Repository;

use AppBundle\Entity\Profile;
use AppBundle\Entity\Role;
use AppBundle\Entity\Status;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use ApiBundle\Request\UpdateProfilePatientRequest;
use ApiBundle\Request\ShowProfileDoctorRequest;

final class ProfileRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var EntityRepository
     */
    private $role;

     /**
     * @var EntityRepository
     */
    private $status;

     /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Profile::class);
        $this->role = $entityManager->getRepository(Role::class);
        $this->status = $entityManager->getRepository(Status::class);
    }
    public function findAll(){
        return $this->repository->findAll();
    }

    public function find($id){
        return $this->repository->find($id);
    }
    public function findByRole($id,$role_id){
        $profile = $this->find($id);
        if(!is_null($profile)){
            $role = $profile->getRole()->getId();
            if($role==$role_id){
                return $profile;
            }
        }
        return null;
    }

    public function isValidEmail($email, $id = null){

        $profile = $this->repository->findOneBy(array('email' => $email));
        if(!($profile !==null && ($id == null || $profile->getId() != $id))){
            return true;
        }
        return false;
    }


    public function updatePatient($profile,UpdateProfilePatientRequest $profilePatient){

        $profile->setFirstName($profilePatient->getFirstName());
        $profile->setSecondName($profilePatient->getSecondName());
        $profile->setFirstSurname($profilePatient->getFirstSurname());
        $profile->setSecondSurname($profilePatient->getSecondSurname());
        $profile->setGender($profilePatient->getGender());
        $profile->setBirthDate($profilePatient->getBirthDate());
        $profile->setTypeDocument($profilePatient->getTypeDocument());
        $profile->setDocumentNumber($profilePatient->getDocumentNumber());
        $profile->setmainPhone($profilePatient->getMainPhone());
        $this->entityManager->flush();

        $profilePatient->setId($profile->getId());
        $profilePatient->setEmail($profile->getEmail());
        $profilePatient->setCreatedAt($profile->getCreatedAt());
        $profilePatient->setUpdatedAt($profile->getUpdatedAt());
        $profilePatient->setProfileImage($profile->getProfileImage());
        return $profilePatient;
    }
    public function transformToPatient(Profile $profile){
        $patient = new UpdateProfilePatientRequest();
        $patient->setFirstName($profile->getFirstName());
        $patient->setSecondName($profile->getSecondName());
        $patient->setFirstSurname($profile->getFirstSurname());
        $patient->setSecondSurname($profile->getSecondSurname());
        $patient->setGender($profile->getGender());
        $patient->setBirthDate($profile->getBirthDate());
        $patient->setTypeDocument($profile->getTypeDocument());
        $patient->setDocumentNumber($profile->getDocumentNumber());
        $patient->setmainPhone($profile->getMainPhone());

        $patient->setId($profile->getId());
        $patient->setEmail($profile->getEmail());
        $patient->setEps($profile->getEps());
        $patient->setProfileImage($profile->getProfileImage());
        $patient->setCreatedAt($profile->getCreatedAt());
        $patient->setUpdatedAt($profile->getUpdatedAt());
        return $patient;
    }

    public function transformToDoctor(Profile $profile){
        $doctor = new ShowProfileDoctorRequest();
        $gender = null;
        if($profile->getGender() == Profile::GENDER_MALE){
            $gender = 1;
        }elseif($profile->getGender() == Profile::GENDER_FEMALE){
            $gender = 1;
       
        }
        $doctor->setId($profile->getId());
        $doctor->setCreatedAt($profile->getCreatedAt());
        $doctor->setUpdatedAt($profile->getUpdatedAt());

        $doctor->setDoctorSpecialties($profile->getDoctorSpecialties());
        return $profile;
    }

    public function setProfileImage($profile,$image_name){
        $profile->setProfileImage($image_name);
        $this->entityManager->flush();
        return $profile;
    }

}