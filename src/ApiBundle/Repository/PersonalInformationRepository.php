<?php 
declare(strict_types=1);

namespace ApiBundle\Repository;

use AppBundle\Entity\Eps;
use AppBundle\Entity\TypeDocument;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class PersonalInformationRepository
{
    /**
     * @var EntityRepository
     */
    private $eps;

    /**
    * @var EntityRepository
    */
    private $typeDocument;


     /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->eps = $entityManager->getRepository(Eps::class);
        $this->typeDocument = $entityManager->getRepository(TypeDocument::class);
    }
    public function findAll(){
        $eps = $this->eps->findAll();
        $typeDocument = $this->typeDocument->findAll();
        $data = [];
        $data['eps']= $eps;
        $data['typeDocument']= $typeDocument;
        return $data;
    }

}