<?php
namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use ApiBundle\Request\UpdateProfilePatientRequest;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Profile;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class ProfilePatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')
        ->add('secondName')
        ->add('firstSurname')
        ->add('gender', ChoiceType::class, array(
            'choices'  => array(
                Profile::GENDER_MALE=>Profile::GENDER_MALE,
                Profile::GENDER_FEMALE =>Profile::GENDER_FEMALE
            ),
        ))
        ->add('secondSurname')
        ->add('mainPhone')
        ->add('birthDate',DateType::class,array(
            'widget'=> 'single_text',
            'format'=> 'yyyy-mm-dd'
        ))
        ->add('eps',EntityType::class, array(
            'class' => 'AppBundle:Eps'
        ))
        ->add('typeDocument',EntityType::class, array(
            'class' => 'AppBundle:TypeDocument'
        ))
        ->add('documentNumber');
    }
  
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            
            'csrf_protection' => false,
            'data_class' => UpdateProfilePatientRequest::class
        ));
    }

}
