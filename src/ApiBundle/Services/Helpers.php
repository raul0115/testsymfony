<?php
namespace ApiBundle\Services;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
class Helpers {


    public function toJson($status = null,$data, $only= null,$ignore =null){
        if(is_null($only)){
            $only = [];
        }
        if(is_null($ignore)){
            $ignore = [];
        }
        $status= $this->getStatus($status);
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        
        if( count($only) < 1){
            $ignore_basic = ["__initializer__", "__cloner__","__isInitialized__"];
            $ignore = array_merge($ignore, $ignore_basic );
            $normalizer->setIgnoredAttributes($ignore);
        }
       
        
		$callbackDate = function($innerObject){
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d'): null;
        };
        $callbackDateTime = function($innerObject){
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d H:i:s'): '';
        };
        $normalizer->setCallbacks(array('birthDate'=> $callbackDate,'createdAt'=> $callbackDateTime,'updatedAt'=> $callbackDateTime));
        $normalizers = array($normalizer);
		$encoders = array("json" => new JsonEncoder());
        $serializer = new Serializer($normalizers, $encoders);
        if($only && count($only) > 0){
            $data = $serializer->normalize($data,null, array('attributes' =>$only));
        }
        $json = $serializer->serialize($data, 'json');
        
        $response = new Response();
		$response->setContent($json);
        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode($status);
		
		return $response;
    }
    
    public function getConstraintViolationList($violations){
        $count = $violations->count();
        $errors = [];
         for ($i = 0; $i < $count; $i++) {
            $name= str_replace("[", "", $violations->get($i)->getPropertyPath());
            $name = str_replace("]", "", $name);
            $errors[$name] = $violations->get($i)->getMessage();
         }
         return $errors;

    }

    public function requestToArray(Request $request){
        if ($this->containsHeader($request, 'Content-Type', 'application/json')) {
            $jsonData = json_decode($request->getContent(), true);
            if ($jsonData) {
                return (array) $jsonData;
            }
        }
       return [];
    }
    /**
     * @param Request $request
     * @param $name
     * @param $value
     * @return bool
     */
    private function containsHeader(Request $request, $name, $value)
    {
        return 0 === strpos($request->headers->get($name), $value);
    }

    public function errorsToArray($form, $object,$except = []){
       
            $errors = [];
            foreach ($object as $key => $value) {
                if(!in_array($key,$except)){
                    $error=$form[$key]->getErrors();
                    if(!is_null($error) && count($error) > 0){
                        $errors[$key]=$error[0]->getMessage();
                    }
                }
               
            }
            if(count($errors) < 1){
                $errors['error']=$form->getErrors()->__toString();
            }
            return $errors;
    }

    private function getStatus($status)
    {
        switch($status){
            case 'validation':
               return Response::HTTP_FORBIDDEN;
               break;
            case 'created':
               return Response::HTTP_CREATED;
               break;
            case 'not_found':
               return Response::HTTP_NOT_FOUND;
               break;
            default:
                return Response::HTTP_OK;
        }
        return Response::HTTP_OK;
    }

    public function str_contains($value, $array)
    {
        foreach ($array as $data) {
            if ($data == $value) {
                return true;
            }
        }

        return false;
    }
    private function getIgnoreAttribute($attribute = [])
    {
       $basic = 
       [
        "__initializer__", 
        "__cloner__",
        "__isInitialized__"
       ];
       foreach ($basic as $data) {
           
        }
    }

	
}
